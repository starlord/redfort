# Red Fort
## Overview

[Redfort](https://codeberg.org/starlord/redfort) will offer a very simple cryptographic harness to java apps. Typical java libraries implementing cryptographic primitives while flexible, are very complex to configure and use. There also exists a gap in fitment of these crypto libraries within java apps like spring boot microservices in container environments. Often developers have to rely on solutions that offered outside standard container orchestration environments which are neither portable nor cheap.

[Redfort](https://codeberg.org/starlord/redfort) sets up crypto drivers with sensible defaults to help developers do cryptographic operations like key generation, encryption, decryption etc. It will support centralized key management and distribution which can be plugged into any server environment.

## Organization

Redfort is opinionated and focused but its organized to easily choose only components that a developer needs. Its separated into:
### 1. Driver

Driver offers a toolbox to configure providers of cryptographic primitives and perform basic cryptographic operations. Driver can do derive keys from password via Argon 2id (default) & PBKDF2 or symmetric keys via ChaCha20 (default) & AES. Driver can also encrypt or decrypt data using ChaCha20-Poly1305 (default) or AES.

Its as simple to use as 3 lines of code to initialize + encrypt clear text/bytes. DRBG algorithm is used for randomness.

### 2. Fortress

The fortress handles Key Management & Distribution, it will offer easy constructs to init & store and share keys in securely using Kerberos protocol. It will not take responsibility to authenticate the actors but will follow the design of TGS and the associated conversation semantics to share keys to encrypt/decrypt clear/cipher text between 2 or more actors. The whole scheme designed to support end to end encrypted transmission via any unsecure channel. It is expected to support various communication topologies from point to point to broadcast.

### 3. Harness

Harness will offer auto-configuration and setup of all this in common frameworks and web environments like Spring.
