package ch.venky.redfort.keygen;

import ch.venky.redfort.util.Toolbox;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.crypto.spec.SecretKeySpec;


@Slf4j
class Argon2KeyGeneratorTest {

    private static final String PASSWORD_OR_SECRET = "venky";
    private static final String SALT = "nacl";
    private static final String EXTRA_SECRET = "the real secret of life";

    @Test
    void deterministicKeyGenerationTest()
    {
        PasswordBasedKeyGenerator argon2KeyGenerator = new Argon2KeyGenerator();
        SecretKeySpec key1 = argon2KeyGenerator.generate(PASSWORD_OR_SECRET, SALT, EXTRA_SECRET);
        log.info("Key:{} of size:{}", Toolbox.encode(key1.getEncoded()), key1.getEncoded().length);

        SecretKeySpec key2 = argon2KeyGenerator.generate(PASSWORD_OR_SECRET, SALT, EXTRA_SECRET);
        log.info("Key:{} of size:{}",Toolbox.encode(key2.getEncoded()), key2.getEncoded().length);

        SecretKeySpec key3 = argon2KeyGenerator.generate(PASSWORD_OR_SECRET, SALT, EXTRA_SECRET);
        log.info("Key:{} of size:{}",Toolbox.encode(key3.getEncoded()), key3.getEncoded().length);

        boolean keyIsSame = Toolbox.encode(key1.getEncoded()).equalsIgnoreCase(Toolbox.encode(key2.getEncoded())) && Toolbox.encode(key2.getEncoded()).equalsIgnoreCase(Toolbox.encode(key3.getEncoded()));

        Assertions.assertTrue(keyIsSame);
    }

    @Test
    void deterministicKeyLengthTest()
    {
        PasswordBasedKeyGenerator argon2KeyGenerator = new Argon2KeyGenerator();
        SecretKeySpec key1 = argon2KeyGenerator.generate(PASSWORD_OR_SECRET, SALT, EXTRA_SECRET);
        log.info("Key:{} of size:{}",Toolbox.encode(key1.getEncoded()), key1.getEncoded().length);

        SecretKeySpec key2 = argon2KeyGenerator.generate(PASSWORD_OR_SECRET, SALT, EXTRA_SECRET);
        log.info("Key:{} of size:{}",Toolbox.encode(key2.getEncoded()), key2.getEncoded().length);

        SecretKeySpec key3 = argon2KeyGenerator.generate(PASSWORD_OR_SECRET, SALT, EXTRA_SECRET);
        log.info("Key:{} of size:{}",Toolbox.encode(key3.getEncoded()), key3.getEncoded().length);


        Assertions.assertTrue(key1.getEncoded().length==key2.getEncoded().length&& key2.getEncoded().length==key3.getEncoded().length && key3.getEncoded().length==32);
    }

}
