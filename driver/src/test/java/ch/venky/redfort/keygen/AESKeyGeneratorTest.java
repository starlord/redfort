package ch.venky.redfort.keygen;

import ch.venky.redfort.util.Toolbox;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.crypto.spec.SecretKeySpec;

@Slf4j
class AESKeyGeneratorTest {

    @Test
    void defaultKeyGenerationTest()
    {
        AESKeyGenerator aesKeyGenerator = new AESKeyGenerator();

        SecretKeySpec key1 = aesKeyGenerator.generate();
        log.info("Key:{} of size:{}", Toolbox.encode(key1.getEncoded()), key1.getEncoded().length);

        SecretKeySpec key2 = aesKeyGenerator.generate();
        log.info("Key:{} of size:{}",Toolbox.encode(key2.getEncoded()), key2.getEncoded().length);


        boolean keyIsSame = Toolbox.encode(key1.getEncoded()).equalsIgnoreCase(Toolbox.encode(key2.getEncoded()));

        Assertions.assertFalse(keyIsSame);
        Assertions.assertTrue(key1.getEncoded().length==key2.getEncoded().length&& key2.getEncoded().length==32);

    }

    @Test
    void declaredKeyGenerationTest()
    {
        AESKeyGenerator aesKeyGenerator = new AESKeyGenerator();

        SecretKeySpec key1 = aesKeyGenerator.generate(256);
        log.info("Key:{} of size:{}", Toolbox.encode(key1.getEncoded()), key1.getEncoded().length);

        SecretKeySpec key2 = aesKeyGenerator.generate(200);
        log.info("Key:{} of size:{}",Toolbox.encode(key2.getEncoded()), key2.getEncoded().length);

        SecretKeySpec key3 = aesKeyGenerator.generate(192);
        log.info("Key:{} of size:{}",Toolbox.encode(key3.getEncoded()), key3.getEncoded().length);

        SecretKeySpec key4 = aesKeyGenerator.generate(128);
        log.info("Key:{} of size:{}",Toolbox.encode(key4.getEncoded()), key4.getEncoded().length);


        boolean keyIsSame = Toolbox.encode(key1.getEncoded()).equalsIgnoreCase(Toolbox.encode(key2.getEncoded())) && Toolbox.encode(key2.getEncoded()).equalsIgnoreCase(Toolbox.encode(key3.getEncoded()));
        Assertions.assertFalse(keyIsSame);

        Assertions.assertTrue(key1.getEncoded().length==key2.getEncoded().length&& key2.getEncoded().length==32);
        Assertions.assertEquals(24, key3.getEncoded().length);
        Assertions.assertEquals(16, key4.getEncoded().length);
    }

}
