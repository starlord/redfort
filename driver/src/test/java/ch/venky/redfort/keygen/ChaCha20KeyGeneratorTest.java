package ch.venky.redfort.keygen;

import ch.venky.redfort.util.Toolbox;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.crypto.spec.SecretKeySpec;

@Slf4j
class ChaCha20KeyGeneratorTest {

    @Test
    void defaultKeyGenerationTest()
    {
        ChaCha20KeyGenerator chaCha20KeyGenerator = new ChaCha20KeyGenerator();

        SecretKeySpec key1 = chaCha20KeyGenerator.generate();
        log.info("Key:{} of size:{}", Toolbox.encode(key1.getEncoded()), key1.getEncoded().length);

        SecretKeySpec key2 = chaCha20KeyGenerator.generate();
        log.info("Key:{} of size:{}",Toolbox.encode(key2.getEncoded()), key2.getEncoded().length);


        boolean keyIsSame = Toolbox.encode(key1.getEncoded()).equalsIgnoreCase(Toolbox.encode(key2.getEncoded()));

        Assertions.assertFalse(keyIsSame);
        Assertions.assertTrue(key1.getEncoded().length==key2.getEncoded().length&& key2.getEncoded().length==32);

    }

}
