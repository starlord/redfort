package ch.venky.redfort.devices;

import ch.venky.redfort.exceptions.CryptoDeviceException;
import ch.venky.redfort.exceptions.DecryptionException;
import ch.venky.redfort.exceptions.EncryptionException;
import ch.venky.redfort.exceptions.KeyDerivationException;
import ch.venky.redfort.keygen.AESKeyGenerator;
import ch.venky.redfort.keygen.BasicKeyGenerator;
import ch.venky.redfort.keygen.ChaCha20KeyGenerator;
import ch.venky.redfort.keygen.PasswordBasedKeyGenerator;
import ch.venky.redfort.util.Toolbox;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.util.Date;

import static ch.venky.redfort.devices.Scrambler.IV_LENGTH;


@Slf4j
class CryptoDriverTest {

    private static final String CLEAR_TEXT = "Liberté, égalité, fraternité";
    private static final byte[] CLEAR_TEXT_BYTES = CLEAR_TEXT.getBytes(StandardCharsets.UTF_8);
    private static final String PASSWORD_OR_SECRET = "venky";
    private static final String SALT = "nacl";
    private static final String EXTRA_SECRET = "the real secret of life";


    @Test
    void chacha20Poly1305OneDeviceTest() {

        CryptoDriver cryptoDriver = CryptoDriver.newPBDriver();
        PasswordBasedKeyGenerator keyGenerator = (PasswordBasedKeyGenerator) cryptoDriver.getKeyGenerator();

        SecretKeySpec key = keyGenerator.generate(PASSWORD_OR_SECRET, SALT, EXTRA_SECRET);

        String cipherText = cryptoDriver.encryptThenEncode(CLEAR_TEXT, key);
        byte[] clearBytes = cryptoDriver.decodeThenDecrypt(cipherText, key);

        log.info("Clear Text:{}",new String(clearBytes));
        Assertions.assertEquals(CLEAR_TEXT, new String(clearBytes));

    }

    @Test
    void chacha20Poly1305TwoDeviceTest() {

        CryptoDriver cryptoDriver1 = CryptoDriver.newPBDriver();
        PasswordBasedKeyGenerator keyGenerator1 = (PasswordBasedKeyGenerator) cryptoDriver1.getKeyGenerator();
        CryptoDriver cryptoDriver2 = CryptoDriver.newPBDriver();

        SecretKeySpec key = keyGenerator1.generate(PASSWORD_OR_SECRET, SALT, EXTRA_SECRET);
        String cipherText = cryptoDriver1.encryptThenEncode(CLEAR_TEXT, key);
        byte[] clearBytes = cryptoDriver2.decodeThenDecrypt(cipherText, key);

        log.info("Clear Text:{}",new String(clearBytes));
        Assertions.assertEquals(CLEAR_TEXT, new String(clearBytes));

    }

    @Test
    void chacha20Poly1305PracticalTestBasic() {

        CryptoDriver cryptoDriver1 = CryptoDriver.newPBDriver();
        PasswordBasedKeyGenerator keyGenerator1 = (PasswordBasedKeyGenerator) cryptoDriver1.getKeyGenerator();
        SecretKeySpec key1 = keyGenerator1.generate(PASSWORD_OR_SECRET, SALT);

        byte[] iv = new byte[IV_LENGTH/8];
        Toolbox.getRandomBitGenerator().nextBytes(iv);

        String cipherTextFrame = cryptoDriver1.getScrambler().encrypt(CLEAR_TEXT_BYTES, key1, iv);
        String cipherText = cipherTextFrame.substring(0, cipherTextFrame.indexOf(Scrambler.CIPHER_TEXT_DELIMITER));

        CryptoDriver cryptoDriver2 = CryptoDriver.newPBDriver();
        PasswordBasedKeyGenerator keyGenerator2 = (PasswordBasedKeyGenerator) cryptoDriver2.getKeyGenerator();
        SecretKeySpec key2 = keyGenerator2.generate(PASSWORD_OR_SECRET, SALT);
        byte[] clearBytes = cryptoDriver2.getScrambler().decrypt(cipherText, key2, Toolbox.encode(iv));

        log.info("Clear Text:{}",new String(clearBytes));
        Assertions.assertEquals(CLEAR_TEXT, new String(clearBytes));

    }

    @Test
    void chacha20Poly1305PracticalTest() {

        CryptoDriver cryptoDriver1 = CryptoDriver.newPBDriver();
        PasswordBasedKeyGenerator keyGenerator1 = (PasswordBasedKeyGenerator) cryptoDriver1.getKeyGenerator();
        SecretKeySpec key1 = keyGenerator1.generate(PASSWORD_OR_SECRET, SALT, EXTRA_SECRET);
        String cipherText = cryptoDriver1.encryptThenEncode(CLEAR_TEXT, key1);

        CryptoDriver cryptoDriver2 = CryptoDriver.newPBDriver();
        PasswordBasedKeyGenerator keyGenerator2 = (PasswordBasedKeyGenerator) cryptoDriver1.getKeyGenerator();
        SecretKeySpec key2 = keyGenerator2.generate(PASSWORD_OR_SECRET, SALT, EXTRA_SECRET);
        byte[] clearBytes = cryptoDriver2.decodeThenDecrypt(cipherText, key2);

        log.info("Clear Text:{}",new String(clearBytes));
        Assertions.assertEquals(CLEAR_TEXT, new String(clearBytes));

    }

    @Test
    void aesGCMOneDeviceTest() {

        CryptoDriver cryptoDriver = CryptoDriver.newDriver(Scrambler.AES_GCM_256, PasswordBasedKeyGenerator.PBKDF2_WITH_HMAC_SHA_512);

        PasswordBasedKeyGenerator keyGenerator1 = (PasswordBasedKeyGenerator) cryptoDriver.getKeyGenerator();
        SecretKeySpec key = keyGenerator1.generate(PASSWORD_OR_SECRET, SALT, EXTRA_SECRET);

        String cipherText = cryptoDriver.encryptThenEncode(CLEAR_TEXT, key);
        byte[] clearBytes = cryptoDriver.decodeThenDecrypt(cipherText, key);

        log.info("Clear Text:{}",new String(clearBytes));
        Assertions.assertEquals(CLEAR_TEXT, new String(clearBytes));

    }

    @Test
    void aesGCMTwoDeviceTest() {

        CryptoDriver cryptoDriver1 = CryptoDriver.newDriver(Scrambler.AES_GCM_256, PasswordBasedKeyGenerator.PBKDF2_WITH_HMAC_SHA_512);
        PasswordBasedKeyGenerator keyGenerator1 = (PasswordBasedKeyGenerator) cryptoDriver1.getKeyGenerator();
        CryptoDriver cryptoDriver2 = CryptoDriver.newDriver(Scrambler.AES_GCM_256, PasswordBasedKeyGenerator.PBKDF2_WITH_HMAC_SHA_512);

        SecretKeySpec key = keyGenerator1.generate(PASSWORD_OR_SECRET, SALT, EXTRA_SECRET);
        String cipherText = cryptoDriver1.encryptThenEncode(CLEAR_TEXT, key);
        byte[] clearBytes = cryptoDriver2.decodeThenDecrypt(cipherText, key);

        log.info("Clear Text:{}",new String(clearBytes));
        Assertions.assertEquals(CLEAR_TEXT, new String(clearBytes));

    }

    @Test
    void aesGCMPracticalTestBasic() {

        CryptoDriver cryptoDriver1 = CryptoDriver.newDriver(Scrambler.AES_GCM_256, PasswordBasedKeyGenerator.PBKDF2_WITH_HMAC_SHA_512);
        PasswordBasedKeyGenerator keyGenerator1 = (PasswordBasedKeyGenerator) cryptoDriver1.getKeyGenerator();
        SecretKeySpec key1 = keyGenerator1.generate(PASSWORD_OR_SECRET, SALT);

        byte[] iv = new byte[IV_LENGTH/8];
        Toolbox.getRandomBitGenerator().nextBytes(iv);

        String cipherTextFrame = cryptoDriver1.getScrambler().encrypt(CLEAR_TEXT_BYTES, key1, iv);
        String cipherText = cipherTextFrame.substring(0, cipherTextFrame.indexOf(Scrambler.CIPHER_TEXT_DELIMITER));

        CryptoDriver cryptoDriver2 = CryptoDriver.newDriver(Scrambler.AES_GCM_256, PasswordBasedKeyGenerator.PBKDF2_WITH_HMAC_SHA_512);
        PasswordBasedKeyGenerator keyGenerator2 = (PasswordBasedKeyGenerator) cryptoDriver2.getKeyGenerator();
        SecretKeySpec key2 = keyGenerator2.generate(PASSWORD_OR_SECRET, SALT);
        byte[] clearBytes = cryptoDriver2.getScrambler().decrypt(cipherText, key2, Toolbox.encode(iv));

        log.info("Clear Text:{}",new String(clearBytes));
        Assertions.assertEquals(CLEAR_TEXT, new String(clearBytes));

    }

    @Test
    void aesGCMPracticalTest() {

        CryptoDriver cryptoDriver1 = CryptoDriver.newDriver(Scrambler.AES_GCM_256, PasswordBasedKeyGenerator.PBKDF2_WITH_HMAC_SHA_512);
        PasswordBasedKeyGenerator keyGenerator1 = (PasswordBasedKeyGenerator) cryptoDriver1.getKeyGenerator();
        SecretKeySpec key1 = keyGenerator1.generate(PASSWORD_OR_SECRET, SALT, EXTRA_SECRET);
        String cipherText = cryptoDriver1.encryptThenEncode(CLEAR_TEXT, key1);

        CryptoDriver cryptoDriver2 = CryptoDriver.newDriver(Scrambler.AES_GCM_256, PasswordBasedKeyGenerator.PBKDF2_WITH_HMAC_SHA_512);
        PasswordBasedKeyGenerator keyGenerator2 = (PasswordBasedKeyGenerator) cryptoDriver2.getKeyGenerator();
        SecretKeySpec key2 = keyGenerator2.generate(PASSWORD_OR_SECRET, SALT, EXTRA_SECRET);
        byte[] clearBytes = cryptoDriver2.decodeThenDecrypt(cipherText, key2);

        log.info("Clear Text:{}",new String(clearBytes));
        Assertions.assertEquals(CLEAR_TEXT, new String(clearBytes));

    }

    @Test
    void aesKeyAESGCMPracticalTest() {

        CryptoDriver cryptoDriver1 = CryptoDriver.newDriver(Scrambler.AES_GCM_256, BasicKeyGenerator.AES);
        AESKeyGenerator keyGenerator1 = (AESKeyGenerator) cryptoDriver1.getKeyGenerator();
        SecretKeySpec key = keyGenerator1.generate();
        String cipherText = cryptoDriver1.encryptThenEncode(CLEAR_TEXT, key);

        CryptoDriver cryptoDriver2 = CryptoDriver.newDriver(Scrambler.AES_GCM_256, BasicKeyGenerator.AES);

        byte[] clearBytes = cryptoDriver2.decodeThenDecrypt(cipherText, key);

        log.info("Clear Text:{}",new String(clearBytes));
        Assertions.assertEquals(CLEAR_TEXT, new String(clearBytes));

    }

    @Test
    void chacha20KeyPracticalTest() {

        CryptoDriver cryptoDriver1 = CryptoDriver.newSymDriver();
        ChaCha20KeyGenerator keyGenerator1 = (ChaCha20KeyGenerator) cryptoDriver1.getKeyGenerator();
        SecretKeySpec key = keyGenerator1.generate();
        String cipherText = cryptoDriver1.encryptThenEncode(CLEAR_TEXT, key);

        CryptoDriver cryptoDriver2 = CryptoDriver.newDriver(Scrambler.CHACHA20_POLY1305, BasicKeyGenerator.CHACHA20);

        byte[] clearBytes = cryptoDriver2.decodeThenDecrypt(cipherText, key);

        log.info("Clear Text:{}",new String(clearBytes));
        Assertions.assertEquals(CLEAR_TEXT, new String(clearBytes));

    }

    @Test
    void defaultScramblerTest() {

        CryptoDriver cryptoDriver1 = CryptoDriver.newPBDriver();
        PasswordBasedKeyGenerator keyGenerator1 = (PasswordBasedKeyGenerator) cryptoDriver1.getKeyGenerator();
        SecretKeySpec key1 = keyGenerator1.generate(PASSWORD_OR_SECRET, SALT);
        String cipherText = cryptoDriver1.encryptThenEncode(CLEAR_TEXT, key1);

        log.info("Cipher Text:{}",cipherText);

        CryptoDriver cryptoDriver2 = CryptoDriver.newPBDriver();
        PasswordBasedKeyGenerator keyGenerator2 = (PasswordBasedKeyGenerator) cryptoDriver2.getKeyGenerator();
        SecretKeySpec key2 = keyGenerator2.generate(PASSWORD_OR_SECRET, SALT);
        byte[] clearBytes = cryptoDriver2.decodeThenDecrypt(cipherText, key2);

        log.info("Clear Text:{}",new String(clearBytes));
        Assertions.assertEquals(CLEAR_TEXT, new String(clearBytes));

    }

    @Test
    void defaultExceptionTest() throws InterruptedException {
        Assertions.assertThrowsExactly(CryptoDeviceException.class, () -> CryptoDriver.newDriver("Serpent", "scrypt"));

        Scrambler scrambler = new Scrambler("Serpent");
        ChaCha20KeyGenerator keyGen = new ChaCha20KeyGenerator();
        CryptoDriver cryptoDriver = new CryptoDriver(scrambler, keyGen);
        SecretKeySpec key = keyGen.generate();

        Assertions.assertThrowsExactly(KeyDerivationException.class, () -> keyGen.derive(512));
        Assertions.assertThrowsExactly(EncryptionException.class, () -> cryptoDriver.encryptThenEncode(CLEAR_TEXT, key));


        scrambler = new Scrambler();
        ChaCha20KeyGenerator keyGenerator = new ChaCha20KeyGenerator();
        CryptoDriver cryptoDriver1 = new CryptoDriver(scrambler, keyGenerator);
        SecretKeySpec key1 = keyGenerator.generate();

        String cipherText = cryptoDriver1.encryptThenEncode(CLEAR_TEXT, key1);

        CryptoDriver cryptoDriver2 = new CryptoDriver(new Scrambler("Serpent"), keyGenerator);

        Assertions.assertThrowsExactly(DecryptionException.class, () -> cryptoDriver2.decodeThenDecrypt(cipherText, key));

        long before = System.currentTimeMillis();
        Thread.sleep(10);
        try {
            cryptoDriver2.decodeThenDecrypt(cipherText, key);
        }
        catch (DecryptionException d)
        {
            Thread.sleep(10);
            long after = System.currentTimeMillis();
            Assertions.assertEquals(d.getCode(), DecryptionException.CODE);
            Assertions.assertTrue(before < d.getTimestamp() && d.getTimestamp() < after);
            Assertions.assertEquals((new Date(d.getTimestamp())).toString(), d.getPrettyStamp());
        }
    }


}
