package ch.venky.redfort.exceptions;


public class DecryptionException extends RunExTemplate
{
    public static String CODE = "0008";
    public DecryptionException(String message) {
        super(CODE, message);
    }
}
