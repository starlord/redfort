package ch.venky.redfort.exceptions;


public class EncryptionException extends RunExTemplate
{
    public static String CODE = "0007";
    public EncryptionException(String message) {
        super(CODE, message);
    }
}
