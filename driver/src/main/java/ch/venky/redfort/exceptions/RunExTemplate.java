package ch.venky.redfort.exceptions;

import java.util.Date;

public abstract class RunExTemplate extends RuntimeException {
    private final String code;
    private final String message;
    private final long timestamp;
    private final String prettyStamp;

    protected RunExTemplate(String code, String message) {
        super(code + ">" + message);
        long now = System.currentTimeMillis();
        this.code = code;
        this.timestamp = now;
        this.prettyStamp = (new Date(now)).toString();
        this.message = code + "::" + message + "; Thrown at raw:" + this.timestamp + ", pretty: " + this.prettyStamp;
    }

    public String getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public String getPrettyStamp() {
        return this.prettyStamp;
    }

    @Override
    public String toString() {
        return "{\"code\":\"" + this.code + '"' + ",\"message\":\"" + this.message + '"' + ",\"rawTimeStamp\":" + this.timestamp + ",\"prettyTimeStamp\":\"" + this.prettyStamp + '"' + '}';
    }
}

