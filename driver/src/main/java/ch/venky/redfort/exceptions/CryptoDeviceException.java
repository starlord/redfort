package ch.venky.redfort.exceptions;


public class CryptoDeviceException extends RunExTemplate
{
    public static String CODE = "0011";
    public CryptoDeviceException(String message) {
        super(CODE, message);
    }
}
