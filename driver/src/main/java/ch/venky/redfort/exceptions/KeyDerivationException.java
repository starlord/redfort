package ch.venky.redfort.exceptions;


public class KeyDerivationException extends RunExTemplate {
    public static String CODE = "0010";
    public KeyDerivationException(String message) {
        super(CODE, message);
    }
}
