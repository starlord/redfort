package ch.venky.redfort.keygen;

import javax.crypto.spec.SecretKeySpec;

public interface SymmetricKeyGenerator extends BasicKeyGenerator
{
    SecretKeySpec generate();

    /**
     * Derives a suitable key from a secret/password, salt, extra secret for keyed hashing. Additional info can be used to tag.
     *
     * @param keyLen length of key supplied scrambler. Take utmost care.
     * @return key that can be wrapped and supplied to Scrambler
     */
    byte[] derive(int keyLen);
}
