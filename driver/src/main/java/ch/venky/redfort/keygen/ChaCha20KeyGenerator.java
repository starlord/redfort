package ch.venky.redfort.keygen;

import ch.venky.redfort.exceptions.KeyDerivationException;
import ch.venky.redfort.util.Toolbox;

import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidParameterException;
import java.security.NoSuchAlgorithmException;

/**
 * Derives a suitable key from a random seed.
 * Uses ChaCha20 by default. Use only when there is an established protocol to exchange keys.
 * For more info look:
 * @see  <a href="https://datatracker.ietf.org/doc/html/rfc8439">rfc8439</a>
 */
public class ChaCha20KeyGenerator implements SymmetricKeyGenerator
{
    private static final String KEY_DERIVATION_FUNCTION = CHACHA20;

    public SecretKeySpec generate()
    {
        return wrap(derive(DEFAULT_KEY_LENGTH));
    }


    public byte[] derive(int keyLen)
    {
        try
        {
            KeyGenerator keyGenerator = KeyGenerator.getInstance(KEY_DERIVATION_FUNCTION);
            keyGenerator.init(keyLen, Toolbox.getRandomBitGenerator());

            return keyGenerator.generateKey().getEncoded();
        }
        catch (NoSuchAlgorithmException | InvalidParameterException e)
        {
            throw new KeyDerivationException(e.getMessage());
        }
    }

    public SecretKeySpec wrap(byte[] rawKey)
    {
        return wrap(rawKey, KEY_DERIVATION_FUNCTION);
    }

    public SecretKeySpec wrap(String rawKey)
    {
        return wrap(Toolbox.decode(rawKey));
    }
}
