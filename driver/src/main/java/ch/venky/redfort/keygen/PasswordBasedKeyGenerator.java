package ch.venky.redfort.keygen;

import javax.crypto.spec.SecretKeySpec;

/**
 * Derives a suitable key from a secret/password, salt, extra secret for keyed hashing. Additional info can be used to tag.
 * WARNING: Stick to {@link Argon2KeyGenerator} and avoid modifying/implementing this or any implementations without understanding the ramifications.
 * For more info:
 * @see <a href="https://datatracker.ietf.org/doc/html/rfc9106">Argon 2id</a>
 * @see <a href="https://nvlpubs.nist.gov/nistpubs/Legacy/SP/nistspecialpublication800-132.pdf">PBKDF2WithHmacSHA512</a>
 */
public interface PasswordBasedKeyGenerator extends BasicKeyGenerator{


    SecretKeySpec generate(String password, String salt);

    SecretKeySpec generate(String password, String salt, String secret);

    SecretKeySpec generate(String password, String salt, String secret, byte[] ad);

    SecretKeySpec generate(char[] password, byte[] salt, byte[] secret, byte[] ad);

    /**
     * Derives a suitable key from a secret/password, salt, extra secret for keyed hashing. Additional info can be used to tag.
     *
     * @param password secret or password that is supplied by user or server. Take utmost care.
     * @param salt Unique value for each secret that's known only to server for each secret
     * @param secret Unique value for each secret that's known only to server
     * @param additional Any additional Tag for ID or classification
     * @return key that can be wrapped and supplied to Scrambler
     */
    byte[] derive(char[] password, byte[] salt, byte[] secret, byte[] additional);


}
