package ch.venky.redfort.keygen;

import ch.venky.redfort.util.Toolbox;
import org.bouncycastle.crypto.generators.Argon2BytesGenerator;
import org.bouncycastle.crypto.params.Argon2Parameters;

import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;

/**
 * Derives a suitable key from a secret/password, salt, extra secret for keyed hashing. Additional info can be used to tag. Implements  {@link PasswordBasedKeyGenerator}
 * WARNING: Stick to {@link Argon2KeyGenerator} and avoid modifying/implementing this or any implementations without understanding the ramifications.
 * For more info see:
 * @see <a href="https://datatracker.ietf.org/doc/html/rfc9106">rfc9106</a>
 */
public class Argon2KeyGenerator implements PasswordBasedKeyGenerator {

    private static final String KEY_DERIVATION_FUNCTION = ARGON2;
    private static final int KEY_LENGTH = 256;
    private static final int ITERATIONS = 3;

    public SecretKeySpec generate(String password, String salt)
    {
        return generate(password, salt,null);
    }

    public SecretKeySpec generate(String password, String salt, String secret)
    {
        return generate(password, salt, secret, null);
    }

    public SecretKeySpec generate(String password, String salt, String secret, byte[] ad)
    {
        return generate(password.toCharArray(), ((salt==null)?null:salt.getBytes(StandardCharsets.UTF_8)), ((secret==null)?null:secret.getBytes(StandardCharsets.UTF_8)), ad);
    }

    public SecretKeySpec generate(char[] password, byte[] salt, byte[] secret, byte[] ad)
    {
        return wrap(derive(password, salt, secret, ad));
    }


    public byte[] derive(char[] password, byte[] salt, byte[] secret, byte[] additional)
    {
        Argon2BytesGenerator bytesGenerator = builder(salt, secret, additional);

        byte[] rawKey = new byte[KEY_LENGTH/8];
        bytesGenerator.generateBytes(password, rawKey);

        return rawKey;
    }

    private Argon2BytesGenerator builder(byte[] salt, byte[] secret, byte[] ad)
    {
        Argon2BytesGenerator bytesGenerator = new Argon2BytesGenerator();
        Argon2Parameters.Builder builder = new Argon2Parameters.Builder(Argon2Parameters.ARGON2_id)
                .withVersion(Argon2Parameters.ARGON2_VERSION_13) // 19
                .withIterations(ITERATIONS)
                .withMemoryAsKB(32)
                .withParallelism(4)
                .withSalt(salt);

        if(secret!=null)
            builder.withSecret(secret);

        if(ad != null)
            builder.withAdditional(ad);


        bytesGenerator.init(builder.build());

        return bytesGenerator;
    }

    public SecretKeySpec wrap(byte[] rawKey)
    {
        return wrap(rawKey, KEY_DERIVATION_FUNCTION);
    }

    public SecretKeySpec wrap(String rawKey)
    {
        return wrap(Toolbox.decode(rawKey));
    }

}
