package ch.venky.redfort.keygen;

import ch.venky.redfort.util.Toolbox;

import javax.crypto.spec.SecretKeySpec;

/**
 * Derives a suitable key from a secret/password, salt, extra secret for keyed hashing. Additional info can be used to tag.
 * WARNING: Stick to {@link Argon2KeyGenerator} and avoid modifying/implementing this or any implementations without understanding the ramifications.
 * For more info:
 * @see <a href="https://datatracker.ietf.org/doc/html/rfc9106">Argon 2id</a>
 * @see <a href="https://nvlpubs.nist.gov/nistpubs/Legacy/SP/nistspecialpublication800-132.pdf">PBKDF2WithHmacSHA512</a>
 */
public interface BasicKeyGenerator {

    String ARGON2 = "Argon2id";
    String PBKDF2_WITH_HMAC_SHA_512 = "PBKDF2WithHmacSHA512";
    String AES = "AES";
    String CHACHA20 = "ChaCha20";

    int DEFAULT_KEY_LENGTH = 256;

    default SecretKeySpec wrap(byte[] rawKey, String keyDeriveFn)
    {
        return new SecretKeySpec(rawKey, keyDeriveFn);
    }

    default SecretKeySpec wrap(String rawKey, String keyDeriveFn)
    {
        return wrap(Toolbox.decode(rawKey), keyDeriveFn);
    }

}
