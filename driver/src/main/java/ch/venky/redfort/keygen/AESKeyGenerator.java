package ch.venky.redfort.keygen;

import ch.venky.redfort.exceptions.KeyDerivationException;
import ch.venky.redfort.util.Toolbox;

import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;
import java.security.NoSuchAlgorithmException;

/**
 * Derives a suitable key from a random seed.
 * Uses ChaCha20 by default. Use only when there is an established protocol to exchange keys.
 * For more info:
 * @see <a href="https://datatracker.ietf.org/doc/html/rfc9106">rfc9106</a>.
 */
public class AESKeyGenerator implements SymmetricKeyGenerator {
    private static final String KEY_DERIVATION_FUNCTION = AES;

    @Override
    public SecretKeySpec generate()
    {
        return generate(DEFAULT_KEY_LENGTH);
    }

    public SecretKeySpec generate(int keyLen)
    {
        return wrap(derive(keyLen));
    }


    public byte[] derive(int keyLen)
    {
        try
        {
            keyLen = (keyLen == 128 || keyLen == 192 || keyLen == 256) ? keyLen : DEFAULT_KEY_LENGTH;
            KeyGenerator keyGenerator = KeyGenerator.getInstance(KEY_DERIVATION_FUNCTION);
            keyGenerator.init(keyLen, Toolbox.getRandomBitGenerator());

            return keyGenerator.generateKey().getEncoded();
        }
        catch (NoSuchAlgorithmException e)
        {
            throw new KeyDerivationException(e.getMessage());
        }
    }

    public SecretKeySpec wrap(byte[] rawKey)
    {
        return wrap(rawKey, KEY_DERIVATION_FUNCTION);
    }

    public SecretKeySpec wrap(String rawKey)
    {
        return wrap(Toolbox.decode(rawKey));
    }

}
