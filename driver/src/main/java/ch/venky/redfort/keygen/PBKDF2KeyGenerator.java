package ch.venky.redfort.keygen;

import ch.venky.redfort.util.Toolbox;
import ch.venky.redfort.exceptions.KeyDerivationException;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;


/**
 * Derives a suitable key from a secret/password, salt, extra secret for keyed hashing. Additional info can be used to tag. Implements {@link PasswordBasedKeyGenerator}.
 * Use this where mandated by law/guidelines.
 * WARNING: Stick to {@link Argon2KeyGenerator} and avoid modifying/implementing this or any implementations without understanding the ramifications.
 * For more info look:
 * @see <a href="https://nvlpubs.nist.gov/nistpubs/Legacy/SP/nistspecialpublication800-132.pdf">NIST800</a>
 */
public class PBKDF2KeyGenerator implements PasswordBasedKeyGenerator {

    private static final String KEY_DERIVATION_FUNCTION = PBKDF2_WITH_HMAC_SHA_512;
    private static final int KEY_LENGTH = 256;
    private static final int ITERATIONS = 3;

    public SecretKeySpec generate(String password, String salt)
    {
        return generate(password, salt,null);
    }

    public SecretKeySpec generate(String password, String salt, String secret)
    {
        return generate(password, salt, secret, null);
    }

    public SecretKeySpec generate(String password, String salt, String secret, byte[] ad)
    {
        return generate(password.toCharArray(), ((salt==null)?null:salt.getBytes(StandardCharsets.UTF_8)), ((secret==null)?null:secret.getBytes(StandardCharsets.UTF_8)), ad);
    }

    public SecretKeySpec generate(char[] password, byte[] salt, byte[] secret, byte[] ad)
    {
        return wrap(derive(password, salt, secret, ad));
    }

    public byte[] derive(char[] password, byte[] salt, byte[] secret, byte[] additional)
    {
        try {
            PBEKeySpec keySpec = new PBEKeySpec(password ,salt, ITERATIONS, KEY_LENGTH);
            SecretKeyFactory pbkdfKeyFactory = SecretKeyFactory.getInstance(KEY_DERIVATION_FUNCTION);

            SecretKey rawKey = pbkdfKeyFactory.generateSecret(keySpec);

            return rawKey.getEncoded();

        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new KeyDerivationException(e.getMessage());
        }

    }

    public SecretKeySpec wrap(byte[] rawKey)
    {
        return wrap(rawKey, KEY_DERIVATION_FUNCTION);
    }

    public SecretKeySpec wrap(String rawKey)
    {
        return wrap(Toolbox.decode(rawKey));
    }

}
