package ch.venky.redfort.devices;


import ch.venky.redfort.exceptions.DecryptionException;
import ch.venky.redfort.exceptions.EncryptionException;
import ch.venky.redfort.util.Toolbox;
import lombok.extern.slf4j.Slf4j;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;

import static ch.venky.redfort.keygen.BasicKeyGenerator.AES;

/**
 * Encrypts and Decrypts data via Symmetric Encryption Cipher Suites.
 * WARNING: Stick to ChaCha20-Poly1305 defaults and avoid modifying/implementing this or any implementations without understanding the ramifications.
 * For further info see:
 * @see <a href="https://csrc.nist.gov/news/2021/call-for-comments-fips-198-1-hash-pubs-and-others">Current NIST Discussions</a>
 * @see <a href="https://datatracker.ietf.org/doc/html/draft-nir-cfrg-chacha20-poly1305-06">ChaCha20-Poly1305</a>}
 * @see <a href="https://nvlpubs.nist.gov/nistpubs/Legacy/SP/nistspecialpublication800-38d.pdf">AES-GCM</a>}
 *
 */
@Slf4j
public class Scrambler {

    public static final String CHACHA20_POLY1305 = "ChaCha20-Poly1305/None/NoPadding";
    public static final String AES_GCM_256 = "AES/GCM/NoPadding";
    public static final int IV_LENGTH = 96;

    public static final String CIPHER_TEXT_DELIMITER = ".";

    public static final String CIPHER_TEXT_DELIMITER_REGEX = "\\.";

    private final String cipherSuite;

    public Scrambler(String cipherSuite)
    {
        this.cipherSuite=cipherSuite;
    }

    public Scrambler()
    {
        this(CHACHA20_POLY1305);
    }

    public String getCipherSuite() {
        return cipherSuite;
    }

    public String encrypt(byte[] clearBytes, SecretKeySpec key)
    {
        return encrypt(clearBytes, key, null);
    }

    /**
     * Encrypts a byte array that is with the supplied key and initialized with manual or internal vector iv using a supported cipher suite
     * @param clearBytes payload to be encrypted
     * @param key base64 encoded key
     * @return byte[] encrypted payload
     */
    public String encrypt(byte[] clearBytes, SecretKeySpec key, byte[] iv)
    {
        byte[] cipherBytes;
        boolean ivNotSupplied = (iv == null);

        try
        {
            Cipher encryptor = Cipher.getInstance(cipherSuite);

            if(ivNotSupplied)
            {
                iv = new byte[IV_LENGTH/8];
                Toolbox.getRandomBitGenerator().nextBytes(iv);
            }

            AlgorithmParameterSpec ivParameterSpec = (AES_GCM_256.equalsIgnoreCase(cipherSuite))?new GCMParameterSpec(IV_LENGTH, iv): new IvParameterSpec(iv);
            key = (AES_GCM_256.equalsIgnoreCase(cipherSuite))? new SecretKeySpec(key.getEncoded(), AES): key;

            encryptor.init(Cipher.ENCRYPT_MODE, key, ivParameterSpec, Toolbox.getRandomBitGenerator());
            cipherBytes = encryptor.doFinal(clearBytes);

            return Toolbox.encode(cipherBytes) + CIPHER_TEXT_DELIMITER + Toolbox.encode(iv);

        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidAlgorithmParameterException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException e) {
            throw new EncryptionException(e.getMessage());
        }

    }

    public byte[] decrypt(String cipherText, SecretKeySpec key)
    {
        if(cipherText.contains(CIPHER_TEXT_DELIMITER))
        {
            String[] cipherBlocks = cipherText.split(CIPHER_TEXT_DELIMITER_REGEX);
            return decrypt(cipherBlocks[0], key, cipherBlocks[1]);
        }
        else
        {
            throw new DecryptionException("IV not available. IV is not supplied with or padded onto cipher text.");
        }
    }
    public byte[] decrypt(String cipherText, SecretKeySpec key, String iv)
    {
        return decrypt(Toolbox.decode(cipherText), key, Toolbox.decode(iv));
    }

    /**
     * Decrypts a byte array that is encrypted with the key and initialized with vector iv using a supported cipher suite
     * @param cipherBytes payload to be encrypted
     * @param key base64 encoded key
     * @return byte[] encrypted payload
     */
    private byte[] decrypt(byte[] cipherBytes, SecretKeySpec key, byte[] iv)
    {
        byte[] clearBytes;
        try
        {
            Cipher decryptor = Cipher.getInstance(cipherSuite);

            AlgorithmParameterSpec ivParameterSpec = (AES_GCM_256.equalsIgnoreCase(cipherSuite))? new GCMParameterSpec(IV_LENGTH, iv): new IvParameterSpec(iv);
            key = (AES_GCM_256.equalsIgnoreCase(cipherSuite))? new SecretKeySpec(key.getEncoded(), AES): key;

            decryptor.init(Cipher.DECRYPT_MODE, key, ivParameterSpec, Toolbox.getRandomBitGenerator());

            clearBytes = decryptor.doFinal(cipherBytes);

        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidAlgorithmParameterException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException e) {
            throw new DecryptionException(e.getMessage());
        }

        return clearBytes;
    }
}
