package ch.venky.redfort.devices;

import ch.venky.redfort.exceptions.CryptoDeviceException;
import ch.venky.redfort.keygen.*;

import javax.crypto.spec.SecretKeySpec;

import java.nio.charset.StandardCharsets;

import static ch.venky.redfort.keygen.BasicKeyGenerator.*;

/**
 * Driver with opinionated defaults to Encrypt and Decrypt data on demand.
 * @since 0.0.1
 */
public class CryptoDriver {

    private final Scrambler scrambler;

    private final BasicKeyGenerator keyGenerator;

    public CryptoDriver(String cipherSuite, String kdf)
    {
        this(newScrambler(cipherSuite), newKeyGenerator(kdf));
    }

    public CryptoDriver(Scrambler scrambler, BasicKeyGenerator keyGenerator)
    {
        this.scrambler = scrambler;
        this.keyGenerator = keyGenerator;
    }

    public Scrambler getScrambler() {
        return scrambler;
    }

    public BasicKeyGenerator getKeyGenerator() {
        return keyGenerator;
    }

    private static Scrambler newScrambler(String cipherSuite)
    {
        if(Scrambler.CHACHA20_POLY1305.equalsIgnoreCase(cipherSuite) || Scrambler.AES_GCM_256.equalsIgnoreCase(cipherSuite))
        {
            return new Scrambler(cipherSuite);
        }
        else
        {
            throw new CryptoDeviceException("Unsupported choice of Cipher Suite. Use "+Scrambler.CHACHA20_POLY1305+" for good security margin");
        }
    }

    private static BasicKeyGenerator newKeyGenerator(String kdf)
    {
        switch (kdf)
        {
            case ARGON2: return new Argon2KeyGenerator();
            case PBKDF2_WITH_HMAC_SHA_512: return new PBKDF2KeyGenerator();
            case AES: return new AESKeyGenerator();
            case CHACHA20: return new ChaCha20KeyGenerator();
            default: throw new CryptoDeviceException("Unsupported choice of KDF. Use "+ ARGON2+" for good security margin");
        }
    }

    public static CryptoDriver newDriver(String cipherSuite, String kdf)
    {
        return new CryptoDriver(cipherSuite, kdf);
    }
    public static CryptoDriver newSymDriver()
    {
        return newDriver(Scrambler.CHACHA20_POLY1305, CHACHA20);
    }
    public static CryptoDriver newPBDriver()
    {
        return newDriver(Scrambler.CHACHA20_POLY1305, ARGON2);
    }

    public String encryptThenEncode(String clearText, SecretKeySpec key)
    {
        return scrambler.encrypt(clearText.getBytes(StandardCharsets.UTF_8), key);
    }

    public byte[] decodeThenDecrypt(String cipherText, SecretKeySpec key)
    {
        return scrambler.decrypt(cipherText, key);
    }

}
