package ch.venky.redfort.util;

import ch.venky.redfort.exceptions.CryptoDeviceException;

import java.nio.charset.StandardCharsets;
import java.security.DrbgParameters;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;

/**
 * ToolBox to share common utils across Scrambler and Key Derivation Implementations
 */
public class Toolbox {

    private Toolbox()
    {
        throw new IllegalStateException("Util class. Do not instantiate.");
    }
    private static final String SOME_HARDCODED_STRING = "scoop-unbounded-riding-unwilling-existing-maverick-cuddly-foil-cvk-repulsion-hardness-humpback";

    public static String encode(byte[] raw)
    {
        return Base64.getEncoder().encodeToString(raw);
    }

    public static byte[] decode(String  raw)
    {
        return Base64.getDecoder().decode(raw);
    }

    /**
     * Generate Secure Random Numbers based on NIST Recommendations. - Venky
     * For further info look:
     * @see <a href="https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.800-90Ar1.pdf">NIST.SP.800</a>
     */
    public static SecureRandom getRandomBitGenerator()
    {
        try {
            return SecureRandom.getInstance("DRBG", DrbgParameters.instantiation(256, DrbgParameters.Capability.PR_AND_RESEED, SOME_HARDCODED_STRING.getBytes(StandardCharsets.UTF_8)));
        } catch (NoSuchAlgorithmException e) {
            throw new CryptoDeviceException(e.getMessage());
        }
    }
}
